### this file takes care of Udev rules, with the file
### /lib/udev/rules.d/99-phoenix.rules
### this file can be useful for packages python3-expeyes and eyes17.
### When a package needs this filoe, it creates it when it is missing
### and adds a comment like # interest_<package-name>

### parameters are package-name and state = enable|disable

package=$1
state=$2

r=/lib/udev/rules.d/99-phoenix.rules

if [ $state = disable ]; then
    i="$(grep interest_$package $r 2>/dev/null)"
    if [ -n "$i" ]; then
	# remove the interest mark from the rules file
	sed -i '/.*interest_'$package'/ d' $r
    fi
    # remove the rules file when there is no more interest in it
    i="$(grep interest $r 2>/dev/null)"
    if [ -z "$i" ]; then
	rm -f $r
    fi
else # assume state = $enable by default
    if [ -f $r ]; then
	i="$(grep interest_$package $r)"
	if [ "no" = "no$i" ]; then
	    # add a mark of interest in an existing rules file
	    # if there is no such mark
	    echo "# interest_$package" >> $r
	fi
    else
	# create a rules file with an interest mark
	cat >$r <<EOF
# udev rules for expEYES interface: AVR, FT232, MCP2200 and CH340
SUBSYSTEM=="usb",ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="21ff", MODE="666"
SUBSYSTEM=="tty",ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", MODE="666"
SUBSYSTEM=="tty",ATTRS{idVendor}=="04d8", ATTRS{idProduct}=="00df", MODE="666"
SUBSYSTEM=="tty",ATTRS{idVendor}=="1a86", ATTRS{idProduct}=="7523", MODE="666"
ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="21ff", ENV{ID_MM_DEVICE_IGNORE}="1"
ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ENV{ID_MM_DEVICE_IGNORE}="1"
ATTRS{idVendor}=="04d8", ATTRS{idProduct}=="00df", ENV{ID_MM_DEVICE_IGNORE}="1"
ATTRS{idVendor}=="1a86", ATTRS{idProduct}=="7523", ENV{ID_MM_DEVICE_IGNORE}="1"
# interest_$package
EOF
    fi
    
fi
